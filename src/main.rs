use std::{collections::{HashMap, HashSet}, env, fmt::{Display, Formatter, Error}, sync::Arc};
use rust_embed::RustEmbed;
use serenity::{
    async_trait,
    client::{Client, Context, EventHandler},
    framework::standard::{
        Args, CommandOptions, CommandResult, CommandGroup,
        macros::{command, group, help, check, hook},
        StandardFramework,
        DispatchError, HelpOptions, help_commands, Reason,
    },
    http::AttachmentType,
    model::{
        id::UserId,
        channel::Message
    },
    Result as SerenityResult,
};
use dotenv;

#[derive(RustEmbed)]
#[folder = "assets/"]
struct Asset;

#[group]
#[commands(ping, vibe, list)]
struct General;

struct Handler;

#[async_trait]
impl EventHandler for Handler {}

#[derive(Debug)]
pub struct VidDeck {
    videos: Vec<&'static str>,
}

impl VidDeck {
    fn contains(&self, name: &str) -> bool {
        if self.videos.iter().any(|&i| i.to_string()==name.to_string()) {
            true
        } else {
            false
        }
    }
}

impl Default for VidDeck {
    fn default() -> Self {
        VidDeck {
            videos: vec![
                "ahh",
                "billy",
                "boo",
                "doll",
                "sexy",
                "tears",
                "west"
            ],
        }
    }
}

impl Display for VidDeck {
    fn fmt(&self, f: &mut Formatter) -> Result<(), Error> {
        let mut comma_separated = String::new();
        for x in &self.videos {
            comma_separated.push_str(&x.to_string());
            comma_separated.push_str(", ");
        }

        comma_separated = comma_separated.trim_end().to_owned();
        comma_separated = comma_separated.trim_end_matches(',').to_owned();
        write!(f, "{}", comma_separated)
    }
}

#[help]
// This replaces the information that a user can pass
// a command-name as argument to gain specific information about it.
#[individual_command_tip =
"Hello! こんにちは！Hola! Bonjour! 您好! 안녕하세요~\n\n\
If you want more information about a specific command, just pass the command as argument."]
// Some arguments require a `{}` in order to replace it with contextual information.
// In this case our `{}` refers to a command's name.
#[command_not_found_text = "Could not find: `{}`."]
async fn my_help(
    context: &Context,
    msg: &Message,
    args: Args,
    help_options: &'static HelpOptions,
    groups: &[&'static CommandGroup],
    owners: HashSet<UserId>
) -> CommandResult {
    let _ = help_commands::with_embeds(context, msg, args, help_options, groups, owners).await;
    Ok(())
}

#[tokio::main]
async fn main() {
    let framework = StandardFramework::new()
        .configure(|c| c.prefix("*"))
        .group(&GENERAL_GROUP)
        .help(&MY_HELP);

    dotenv::dotenv().ok();
    let token = dotenv::var("DISCORD_TOKEN").expect("token");
    let mut client = Client::builder(token)
        .event_handler(Handler)
        .framework(framework)
        .await
        .expect("Error creating client");

    if let Err(why) = client.start().await {
        println!("An error occurred while running the client: {:?}", why);
    }
}

#[command]
async fn ping(context: &Context, msg: &Message) -> CommandResult {
    check_msg(msg.channel_id.say(&context.http, "Pong!").await);
    Ok(())
}

#[command]
async fn list(context: &Context, msg: &Message) -> CommandResult {
    check_msg(msg.channel_id.say(&context.http, format!("{}", VidDeck::default())).await);
    Ok(())
}

#[command]
async fn vibe(context: &Context, msg: &Message, args: Args) -> CommandResult {
    let vid_name = &args.rest();
    let deck = VidDeck::default();
    if !deck.contains(&vid_name) {
        check_msg(
            msg.channel_id
                .send_message(&context.http, |m| {
                    m.content(format!("Sorry, no vibe found called {}", vid_name));
                    m
                })
                .await,
        );
        return Ok(());
    } else {
        check_msg(
            msg.channel_id
                .send_message(&context.http, |m| {
                    m.content(format!("Loading vibe {}", vid_name));
                    m
                })
                .await,
        );
    }
    let vid_full_name = format!("{}.mp4", vid_name);
    let vid = Asset::get(&vid_full_name).unwrap();
    check_msg(
        msg.channel_id
            .send_message(&context.http, |m| {
                m.content("Let's Vibe");
                m.add_file(AttachmentType::from((vid.as_ref(), vid_full_name.as_ref())));
                m
            })
            .await,
    );
    Ok(())
}

fn check_msg(result: SerenityResult<Message>) {
    if let Err(why) = result {
        println!("Error sending message: {:?}", why);
    }
}
